# Image Sequence Director

## Description
Script to play an image sequence inside the StreamingAssets folder. Script has public methods to play, pause and stop the sequence. It has settings to enable loop, play backwards or stop at a certain frame.
Supported types: JPG and PNG.

## Visuals
![](images/screenshot-01.jpg)
![](images/screenshot-02.jpg)

## Installation
Drag the script on a GameObject inside of a Canvas. If the GameObject has an Image component then no Image must be referenced. Create a StreamingAssets folder, if you don't have one inside the Assets folder (Assets/StreamingAsstes) and add your images.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## License
Apache-2.0
https://www.apache.org/licenses/LICENSE-2.0

## Project status
In development.
