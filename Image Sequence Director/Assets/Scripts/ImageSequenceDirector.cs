using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.UI;

public class ImageSequenceDirector : MonoBehaviour
{
	[SerializeField] private bool _playOnAwake = true;
	[SerializeField] private bool _loop = true;
	[SerializeField] private bool _backwards = false;
	[SerializeField] private int _fps = 24;
	[SerializeField] private int _startFrame = 1;
	[SerializeField] private Image _image;

	[Header("Image path in StreamingAssets-Folder")]
	[SerializeField] private string _imagePath = "/ImageSequence/";

	[Header("Infos")]
	[SerializeField] private bool _imagesLoaded = false;
	[SerializeField] private bool _isPlaying = false;
	[SerializeField] private int _frame = 1;
	[SerializeField] private int _frameCount;

	private string _folderPath;
	private string[] _filePaths;
	private Sprite[] _sprites;
	private Coroutine _playCoroutine;

	private event Action onImagesLoaded;

	#region Properties

	public int frame
	{
		get => _frame;
		set
		{
			_frame = Mathf.Clamp(value, 1, _frameCount);
			SetImage(_frame);
		}
	}

	public bool imagesLoaded { get => _imagesLoaded; }

	#endregion Properties

	private void Awake()
	{
		if (_image == null)
		{
			if (!TryGetComponent<Image>(out _image))
			{
				Debug.LogError("No Image component found!");
			}
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		LoadImagesFromStreamingAssets();
		onImagesLoaded += HandleOnImagesLoaded;
	}

	private void HandleOnImagesLoaded()
	{
		if (_playOnAwake)
		{
			Play();
		}
	}

	private void OnDestroy()
	{
		if (_playCoroutine != null) StopCoroutine(_playCoroutine);
	}

	private void LoadImagesFromStreamingAssets()
	{
		// Get path of folder
		_folderPath = Application.streamingAssetsPath + _imagePath;

		// Collect images paths
		// All must be JPG or PNG
		// Cannot be mixed
		_filePaths = System.IO.Directory.GetFiles(_folderPath, "*.jpg"); // Get all files of type .jpg in this folder
		if (_filePaths.Length == 0)
		{
			// Try it with .png
			_filePaths = System.IO.Directory.GetFiles(_folderPath, "*.png"); // Try to get all files of type .png in this folder
		}

		if (_filePaths.Length > 0)
		{
			StartCoroutine(IELoadImagesFromStreamingAssets());
		}
	}

	private IEnumerator IELoadImagesFromStreamingAssets()
	{
		_frameCount = _filePaths.Length;
		_sprites = new Sprite[_frameCount];

		for (int i = 0; i < _frameCount; i++)
		{
			// Converts desired path into byte array
			byte[] pngBytes = System.IO.File.ReadAllBytes(_filePaths[i]);

			// Creates texture and loads byte array data to create image
			Texture2D tex = new Texture2D(2, 2);
			tex.LoadImage(pngBytes);

			// Creates a new Sprite based on the Texture2D
			Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

			// Save sprite in array
			_sprites[i] = fromTex;

			yield return null;
		}

		// Assign first frame/sprite
		frame = _startFrame;

		_imagesLoaded = true;
		onImagesLoaded?.Invoke();
		Debug.Log("<color=lime>ImageSequenceDirector: Finished loading all images from StreamingAssets</color>");
	}

	private IEnumerator IEPlaying(int stopAtFrame = -1)
	{
		_isPlaying = true;

		while (_isPlaying)
		{
			// Forward
			if (!_backwards)
			{
				// Last frame reached
				if (_frame == _frameCount)
				{
					if (_loop) frame = 1;
					else Pause();
				}
				else
				{
					frame++;
				}
			}
			// Backward
			else
			{
				// First frame reached
				if (_frame == 1)
				{
					if (_loop) frame = _frameCount;
					else Pause();
				}
				else
				{
					frame--;
				}
			}

			// Check if it should be paused
			if (stopAtFrame >= 0)
			{
				if (frame == stopAtFrame) Pause();
			}
			yield return new WaitForSeconds(1f/_fps);
		}
	}

	#region Public methods

	public void Play(int stopAtFrame = -1)
	{
		if (_imagesLoaded)
		{
			if (!_isPlaying)
			{
				if (_playCoroutine != null) StopCoroutine(_playCoroutine);
				_playCoroutine = StartCoroutine(IEPlaying(stopAtFrame));
			}
		}
		else
		{
			Debug.LogWarning("Images not loaded completed. This might lead to unwanted behaviour!");
		}

	}

	public void Pause()
	{
		_isPlaying = false;
	}

	public void Stop()
	{
		Pause();
		frame = 1;
		if (_playCoroutine != null) StopCoroutine(_playCoroutine);
	}

	public void SetImage(int frame)
	{
		int index = frame - 1;
		_image.sprite = _sprites[index];
	}

	#endregion Public methods
}
